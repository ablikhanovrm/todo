import {
    DocumentType,
    getModelForClass,
    prop,
    ReturnModelType,
  } from '@typegoose/typegoose';
  

export class TaskSchema {
    @prop({ required: true, type: String })
    public taskText: string;
}

export const Task = getModelForClass(TaskSchema);
export type TaskDocumentType = DocumentType<TaskSchema>;
export type TaskModelType = ReturnModelType<typeof TaskSchema>;
