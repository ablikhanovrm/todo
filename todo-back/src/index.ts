import express, { Request, Response } from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import { graphqlHTTP } from 'express-graphql';
import schema from './schema';
import mongoose from 'mongoose';


dotenv.config();
const port = process.env.PORT;
const mongoUrl = process.env.MONGO_URL;
const databaseName = process.env.MONGO_DATABASE_NAME;
const app = express();
app.use(cors());
app.use('/graphql', graphqlHTTP({
    graphiql: true,
    schema: schema,
}))


app.get('/', (req: Request, res: Response) => {
    res.send('Express + TypeScript Server');
  });
  
  const run = async () => {

      if (mongoUrl) {
        mongoose.connect(`${mongoUrl}/${databaseName}`);
      } else {
        throw new Error('Can not connect database');
      }
  
      app.listen(port, () => console.log(`Server started on port ${port}`));
 
      process.on('exit', () => {
        mongoose.disconnect();
      });
    }
  
  run().catch(console.error);
  
