import { composeMongoose } from 'graphql-compose-mongoose';
import { Task } from '../../models/Task';

export const TaskTC = composeMongoose(Task)