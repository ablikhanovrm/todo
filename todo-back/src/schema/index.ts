import { schemaComposer } from 'graphql-compose';
import { Task } from '../models/Task';
import { TaskTC } from './entities/TaskTC';

schemaComposer.Query.addFields({
  getAllTasks: {
    type: TaskTC.List,
    resolve: async () => {
      const tasks = await Task.find();
      return tasks;
    },
  },
});

schemaComposer.Mutation.addFields({
  createTask: {
    type: TaskTC,
    args: { input: schemaComposer.createInputTC({ name: 'Task', fields: { taskText: 'String' } }) },
    resolve: async (_, { input }) => {
      return await Task.create(input);
    },
  },
  deleteTask: {
    type: TaskTC,
    args: { input: schemaComposer.createInputTC({ name: 'TaskId', fields: { _id: 'String' } }) },
    resolve: async (_, { input }) => {
      await Task.findByIdAndDelete({ _id: input._id });
      return { _id: input._id };
    },
  },
});

const schema = schemaComposer.buildSchema();

export default schema;
