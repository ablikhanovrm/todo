"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_graphql_1 = require("express-graphql");
const schema_1 = __importDefault(require("./schema"));
const mongoose_1 = __importDefault(require("mongoose"));
dotenv_1.default.config();
const port = process.env.PORT;
const mongoUrl = process.env.MONGO_URL;
const databaseName = process.env.MONGO_DATABASE_NAME;
const app = (0, express_1.default)();
app.use((0, cors_1.default)());
app.use('/graphql', (0, express_graphql_1.graphqlHTTP)({
    graphiql: true,
    schema: schema_1.default,
}));
app.get('/', (req, res) => {
    res.send('Express + TypeScript Server');
});
const run = () => __awaiter(void 0, void 0, void 0, function* () {
    if (mongoUrl) {
        mongoose_1.default.connect(`${mongoUrl}/${databaseName}`);
    }
    else {
        throw new Error('Can not connect database');
    }
    app.listen(port, () => console.log(`Server started on port ${port}`));
    process.on('exit', () => {
        mongoose_1.default.disconnect();
    });
});
run().catch(console.error);
//# sourceMappingURL=index.js.map