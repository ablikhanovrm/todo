"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaskTC = void 0;
const graphql_compose_mongoose_1 = require("graphql-compose-mongoose");
const Task_1 = require("../../models/Task");
exports.TaskTC = (0, graphql_compose_mongoose_1.composeMongoose)(Task_1.Task);
//# sourceMappingURL=TaskTC.js.map