"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_compose_1 = require("graphql-compose");
const Task_1 = require("../models/Task");
const TaskTC_1 = require("./entities/TaskTC");
graphql_compose_1.schemaComposer.Query.addFields({
    getAllTasks: {
        type: TaskTC_1.TaskTC.List,
        resolve: () => __awaiter(void 0, void 0, void 0, function* () {
            const tasks = yield Task_1.Task.find();
            return tasks;
        }),
    },
});
graphql_compose_1.schemaComposer.Mutation.addFields({
    createTask: {
        type: TaskTC_1.TaskTC,
        args: { input: graphql_compose_1.schemaComposer.createInputTC({ name: 'Task', fields: { taskText: 'String' } }) },
        resolve: (_, { input }) => __awaiter(void 0, void 0, void 0, function* () {
            return yield Task_1.Task.create(input);
        }),
    },
    deleteTask: {
        type: TaskTC_1.TaskTC,
        args: { input: graphql_compose_1.schemaComposer.createInputTC({ name: 'TaskId', fields: { _id: 'String' } }) },
        resolve: (_, { input }) => __awaiter(void 0, void 0, void 0, function* () {
            yield Task_1.Task.findByIdAndDelete({ _id: input._id });
            return { _id: input._id };
        }),
    },
});
const schema = graphql_compose_1.schemaComposer.buildSchema();
exports.default = schema;
//# sourceMappingURL=index.js.map