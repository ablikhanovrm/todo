import { DocumentType, ReturnModelType } from '@typegoose/typegoose';
export declare class TaskSchema {
    taskText: string;
}
export declare const Task: ReturnModelType<typeof TaskSchema, import("@typegoose/typegoose/lib/types").BeAnObject>;
export type TaskDocumentType = DocumentType<TaskSchema>;
export type TaskModelType = ReturnModelType<typeof TaskSchema>;
//# sourceMappingURL=Task.d.ts.map