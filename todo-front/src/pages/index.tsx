import {CreateTaskForm} from "../components/CreateTaskForm/CreateTaskForm"
import { Tasks } from "../components/Tasks/Tasks"


export default function Home() {

  return (
    <>
      <h1 style={{textAlign: 'center', margin: 0, marginTop:'20px'}}>Todo List</h1>
      <CreateTaskForm/>
      <Tasks/>
    </>
  )
}
