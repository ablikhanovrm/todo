import { DeleteOutlined } from "@ant-design/icons";
import { useMutation } from "@apollo/client";
import { Button } from "antd";
import { DELETE_TASK } from "./DeleteTaskButtonMutation";

interface Props {
    id: string
}

export function DeleteButton( {id}: Props ){
    const [deleteTask] = useMutation(DELETE_TASK);
    console.log(id);
    

    const deleteTaskHandler = (id: string) => {
        console.log(id,'klk');
        
       deleteTask({
        variables: {
            input: { _id:id}
        },
        refetchQueries:['GetAllTasksQuery']
       });
      };

    return (
    <Button size='small' onClick={() => deleteTaskHandler(id)}>
        <DeleteOutlined style={{ color: 'red', fontSize: '16px' }} />
    </Button>

    )
}