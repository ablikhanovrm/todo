import { gql } from "@apollo/client/core";

export const DELETE_TASK = gql`
  mutation DeleteTaskButtonMutation($input: TaskId){
    deleteTask(input: $input){
      _id
    }
  }
`;