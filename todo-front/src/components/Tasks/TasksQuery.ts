import { gql } from "@apollo/client";

export const GET_ALL_TASKS = gql`
    query GetAllTasksQuery {
        getAllTasks {
            _id, 
            taskText
        }
    }
`