import { Typography } from 'antd';
import { DeleteButton } from '../../DeleteButton/DeleteButton';

export interface ITask {
  _id: string;
  taskText: string;
}

interface TaskProps {
  task: ITask;
}

export function TaskParagraph(taskProps: TaskProps) {
  const { task } = taskProps;
  const { Text, Paragraph } = Typography;
  console.log({ task });

  return (
    <Paragraph
      style={{
        border: '1px solid black',
        width: '300px',
        height: '60px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: '10px',
        padding: '16px',
      }}
    >
      <Text key={task?._id}>{task.taskText}</Text>
      <DeleteButton id={task._id} />
    </Paragraph>
  );
}
