import { useQuery } from "@apollo/client";
import { GET_ALL_TASKS } from "./TasksQuery";
import { TaskParagraph, ITask } from "./Task/TaskParagraph";

export function Tasks() {
  
  const { data } = useQuery(GET_ALL_TASKS)
  const tasks:ITask[] = data?.getAllTasks || [] 

    return (
        <>
        {
          tasks.length < 0 ? 
          <h3>Нет активных задач</h3>
          :
          tasks.map(task => 
            <div key={task._id} style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
              <TaskParagraph task={task}/>
            </div>
          )
        }
        </>
    )
  }