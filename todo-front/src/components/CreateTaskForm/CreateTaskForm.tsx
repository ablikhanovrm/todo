import { useMutation } from '@apollo/client';
import { Button, Form, Input } from 'antd';
import { ChangeEvent, useState } from 'react';
import {CREATE_TASK} from './CreateTaskFormMutation';

export function CreateTaskForm(){
  
  const [taskText, setTaskText] = useState('');
  const [createTask] = useMutation(CREATE_TASK);

  const inputChangeHandler = (e:ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setTaskText(value);
  };

  const onFinish = () => {
    createTask({
      variables:{
        input: {
          taskText
        }
      },  refetchQueries:['GetAllTasksQuery']
    })
    setTaskText('');
  };

  return (
    <Form
      name="basic"
      style={{display:'flex', justifyContent:'center', marginBottom: '20px', marginTop: '30px'}}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item
        name="taskText"
        rules={[{ required: true, message: 'Please input some text' }]}
      >
        <Input 
            name="taskText"
            onChange={inputChangeHandler}
            value={taskText}
            size='large'
            style={{width:'400px'}}
        />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Create
        </Button>
      </Form.Item>
    </Form>
  );
};
