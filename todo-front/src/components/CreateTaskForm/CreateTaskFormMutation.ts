import { gql } from "@apollo/client";

export const CREATE_TASK = gql`
    mutation CreateTaskFormMutation($input: Task) {
        createTask(input: $input) {
            _id, taskText
        }
    }
`;